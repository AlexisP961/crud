package com.adpg.crud.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.adpg.crud.domain.Persona;

@Repository
public interface PersonaRepository extends JpaRepository<Persona, Long> {

}
