package com.adpg.crud.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adpg.crud.domain.Persona;
import com.adpg.crud.repository.PersonaRepository;

@Service
public class PersonaService  {
	
	@Autowired
	private PersonaRepository personaRepository;

	public List<Persona> getAllPersonas() {
		return personaRepository.findAll();
	}
	
	public Persona getOneById(Long id) {
		return personaRepository.getOne(id);
	}
	
	public Persona addPersona(Persona persona) {
		return personaRepository.save(persona);
	}
	
	public void updatePersona(Persona persona) {
		personaRepository.saveAndFlush(persona);
	}
	
	public void deletePersonaById(Long id) {
		personaRepository.deleteById(id);
	}

}
