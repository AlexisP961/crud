package com.adpg.crud.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.adpg.crud.domain.Persona;
import com.adpg.crud.service.PersonaService;

@RestController
public class PersonaController {
	
	@Autowired
	private PersonaService personaService;
	
	@RequestMapping(value = "/personas", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
	public List<Persona> getAllPersona(){
		return personaService.getAllPersonas();
	}
	
	@RequestMapping(value = "/persona/save", method = RequestMethod.POST, produces = "application/json; charset=utf-8")
	public @ResponseBody Persona saveSite(@RequestBody Persona persona) {
		return personaService.addPersona(persona);
	}
	
	
	
}
